<?php

include_once '../startup.php';
use App\User\Auth;
use App\Utility\Direction;

$objAuth = new Auth();
$status = $objAuth->is_loggedin();

if($status == false){
    return Direction::redirect("../../index.php"); 
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Create New Contact</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php require_once('../Layout/common_style.php'); ?>
        <link rel="stylesheet" href="../../resource/css/fileinput.min.css">
    </head>

    <body>

        <?php require_once('../Layout/navbar.php'); ?>

        <div class="container">
            <h1 class="text-center">Create New Contact</h1>
            <hr/>
            <div class="jumbotron col-md-8 col-md-offset-2">
                <form role="form" action="store.php" method="POST" enctype="multipart/form-data">
                    <h3>Personal</h3>
                    <input type="hidden" name="added_by" value="<?= $_SESSION['userid']; ?>"/>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="first-name">First Name: </label><span style="color: red;"> &starf;</span>
                                <input type="text" name="name[]" class="form-control" id="first-name" autofocus="autofocus" required="required" placeholder="Enter your first name">
                            </div>
                            <div class="col-md-6">
                                <label for="last-name">Last Name:</label><span style="color: red;"> &starf;</span>
                                <input type="text" name="name[]" class="form-control" id="last-name" required="required" placeholder="Enter your last name">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="profile_picture">Choose Profile Picture:</label>
                        <input type="file" name="profile_picture" class="file" id="profile_picture">
                        <small><i>(file size mustn't exceed 200kb. jpg/png allowed)</i></small>
                    </div>
                   
                    <h3>Contact</h3>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="mobile">Mobile: </label><span style="color: red;"> &starf;</span>
                                <input type="tel" name="mobile" class="form-control" id="mobile" required="required">
                            </div>
                            <div class="col-md-6">
                                <label for="group">Contact Group:</label>
                                <select class="form-control" name="group" id="group">
                                    <option value="Friends">Friends</option>
                                    <option value="Family">Family</option>
                                    <option value="Work">Work</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group pull-right">
                        <a href="javascript:history.go(-1)" class="btn btn-primary"><span class="glyphicon glyphicon-menu-left"></span> Back</a>
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-user"></span> Create</button>
                    </div>
                </form>
            </div>
        </div>

        <?php require_once('../Layout/footer.php'); ?>
        <?php require_once('../Layout/common_script.php'); ?>
        <script src="../../resource/js/fileinput.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#profile_picture").fileinput({
                    'allowedFileExtensions' : ['jpg', 'png'],
                    'maxFileSize': 200
                });

                $('.alert').fadeOut(4000);
            });
        </script>
    </body>
</html>