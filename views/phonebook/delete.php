<?php

include_once '../startup.php';
use App\User\Auth;
use App\Utility\Direction;
use App\Contact\Phonebook;

$objAuth = new Auth();
$status = $objAuth->is_loggedin();

if($status == false){
    return Direction::redirect("../../index.php"); 
} else{
    $objContact = new Phonebook();
    $objContact->delete($_POST['id']);
}